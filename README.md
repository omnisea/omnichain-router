This repository contains the core smart contracts for the Omnisea's Omnichain Router.
For the Omnisea Omnichain NFTs contracts, see the [omnisea-contracts](https://gitlab.com/omnisea/contracts)
repository.

## Licensing

The primary license for Omnichain Router is the Business Source License 1.1 (`BUSL-1.1`), see [`LICENSE`](./LICENSE).
