const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

const network = 'moonbeam_testnet';
const networkToDeployParamsMap = {
    rinkeby: {
        lzEndpoint: '0xbfD2135BFfbb0B5378b56643c2Df8a87552Bfa23',
        axGateway: '0x79a63d6d8BBD5c6dfc774dA79bCcD948EAcb53FA', // Non-exist on Rinkeby
        axGasReceiver: '0x79a63d6d8BBD5c6dfc774dA79bCcD948EAcb53FA', // Non-exist on Rinkeby
    },
    bsc_testnet: {
        lzEndpoint: '0x6Fcb97553D41516Cb228ac03FdC8B9a0a9df04A1',
        axGateway: '0x4D147dCb984e6affEEC47e44293DA442580A3Ec0',
        axGasReceiver: '0xbE406F0189A0B4cf3A05C286473D23791Dd44Cc6',
    },
    avalanche_testnet: {
        lzEndpoint: '0x93f54D755A063cE7bB9e6Ac47Eccc8e33411d706',
        axGateway: '0xC249632c2D40b9001FE907806902f63038B737Ab',
        axGasReceiver: '0xbE406F0189A0B4cf3A05C286473D23791Dd44Cc6',
    },
    polygon_testnet: {
        lzEndpoint: '0xf69186dfBa60DdB133E91E9A4B5673624293d8F8',
        axGateway: '0xBF62ef1486468a6bd26Dd669C06db43dEd5B849B',
        axGasReceiver: '0xbE406F0189A0B4cf3A05C286473D23791Dd44Cc6',
    },
    optimism_testnet: {
        lzEndpoint: '0xae92d5aD7583AD66E49A0c67BAd18F6ba52dDDc1',
        axGateway: '0x72aB53a133b27Fa428ca7Dc263080807AfEc91b5', // Non-exist on Optimism
        axGasReceiver: '0x72aB53a133b27Fa428ca7Dc263080807AfEc91b5', // Non-exist on Optimism
    },
    arbitrum_testnet: {
        lzEndpoint: '0x6aB5Ae6822647046626e83ee6dB8187151E1d5ab',
        axGateway: '0x72aB53a133b27Fa428ca7Dc263080807AfEc91b5', // Non-exist on Arbitrum
        axGasReceiver: '0x72aB53a133b27Fa428ca7Dc263080807AfEc91b5', // Non-exist on Arbitrum
    },
    ftm_testnet: {
        lzEndpoint: '0x7dcAD72640F835B0FA36EFD3D6d3ec902C7E5acf',
        axGateway: '0x97837985Ec0494E7b9C71f5D3f9250188477ae14',
        axGasReceiver: '0xbE406F0189A0B4cf3A05C286473D23791Dd44Cc6',
    },
    moonbeam_testnet: {
        lzEndpoint: '0xb23b28012ee92E8dE39DEb57Af31722223034747',
        axGateway: '0x5769D84DD62a6fD969856c75c7D321b84d455929',
        axGasReceiver: '0xbE406F0189A0B4cf3A05C286473D23791Dd44Cc6',
    },
    harmony_testnet: {
        lzEndpoint: '0xae92d5aD7583AD66E49A0c67BAd18F6ba52dDDc1',
        axGateway: '0x5769D84DD62a6fD969856c75c7D321b84d455929',
        axGasReceiver: '0xbE406F0189A0B4cf3A05C286473D23791Dd44Cc6',
    },

    // !!! MAINNET NETWORKS!!!

    // mainnet: {
    //     lzEndpoint: '0x66A71Dcef29A0fFBDBE3c6a460a3B5BC225Cd675',
    //     axGateway: '0x4F4495243837681061C4743b74B3eEdf548D56A5',
    //     axGasReceiver: '0x4154CF6eea0633DD9c4933E76a077fD7E9260738',
    // },
    // bsc_MAINNET: {
    //     lzEndpoint: '0x3c2269811836af69497E5F486A85D7316753cf62',
    //     axGateway: '0x304acf330bbE08d1e512eefaa92F6a57871fD895',
    //     axGasReceiver: '0x2d5d7d31F671F86C782533cc367F14109a082712',
    // },
    // avalanche_MAINNET: {
    //     lzEndpoint: '0x3c2269811836af69497E5F486A85D7316753cf62',
    //     axGateway: '0x5029C0EFf6C34351a0CEc334542cDb22c7928f78',
    //     axGasReceiver: '0xB53C693544363912D2A034f70D9d98808D5E192a',
    // },
    // polygon_MAINNET: {
    //     lzEndpoint: '0x3c2269811836af69497E5F486A85D7316753cf62',
    //     axGateway: '0x6f015F16De9fC8791b234eF68D486d2bF203FBA8',
    //     axGasReceiver: '0xc8E0b617c388c7E800a7643adDD01218E14a727a',
    // },
    // arbitrum_MAINNET: {
    //     lzEndpoint: '0x3c2269811836af69497E5F486A85D7316753cf62',
    //     axGateway: '0x3c2269811836af69497E5F486A85D7316753cf62', // not exists
    //     axGasReceiver: '0x3c2269811836af69497E5F486A85D7316753cf62', // not exists
    // },
    // optimism_MAINNET: {
    //     lzEndpoint: '0x3c2269811836af69497E5F486A85D7316753cf62',
    //     axGateway: '0x3c2269811836af69497E5F486A85D7316753cf62', // not exists
    //     axGasReceiver: '0x3c2269811836af69497E5F486A85D7316753cf62', // not exists
    // },
    // ftm_MAINNET: {
    //     lzEndpoint: '0xb6319cC6c8c27A8F5dAF0dD3DF91EA35C4720dd7',
    //     axGateway: '0x304acf330bbE08d1e512eefaa92F6a57871fD895',
    //     axGasReceiver: '0x2879da536D9d107D6b92D95D7c4CFaA5De7088f4',
    // },
    // swimmer_MAINNET: {
    //     lzEndpoint: '0x9740FF91F1985D8d2B71494aE1A2f723bb3Ed9E4',
    //     axGateway: '0x9740FF91F1985D8d2B71494aE1A2f723bb3Ed9E4', // not exists
    //     axGasReceiver: '0x9740FF91F1985D8d2B71494aE1A2f723bb3Ed9E4', // not exists
    // },
    // harmony_MAINNET: {
    //     lzEndpoint: '0x9740FF91F1985D8d2B71494aE1A2f723bb3Ed9E4',
    //     axGateway: '0x9740FF91F1985D8d2B71494aE1A2f723bb3Ed9E4', // not exists
    //     axGasReceiver: '0x2d5d7d31F671F86C782533cc367F14109a082712', // not exists
    // },
    // moonbeam_MAINNET: {
    //     lzEndpoint: '0x9740FF91F1985D8d2B71494aE1A2f723bb3Ed9E4',
    //     axGateway: '0x4F4495243837681061C4743b74B3eEdf548D56A5',
    //     axGasReceiver: '0x2d5d7d31F671F86C782533cc367F14109a082712',
    // },
};

async function main() {
    console.log('deploying');
    const networkParams = networkToDeployParamsMap[network];
    const OmnichainRouter = await hre.ethers.getContractFactory("OmnichainRouter");
    const omnichainRouter = await OmnichainRouter.deploy(networkParams.lzEndpoint, networkParams.axGateway, networkParams.axGasReceiver);
    await omnichainRouter.deployed();
    console.log("OmnichainRouter deployed to:", omnichainRouter.address);

    await (() => {
        return new Promise(async (resolve, reject) => {
            setTimeout(async () => {
                try {
                    await hre.run("verify:verify", {
                        address: omnichainRouter.address,
                        constructorArguments: [
                            networkParams.lzEndpoint,
                            networkParams.axGateway,
                            networkParams.axGasReceiver,
                        ],
                    });

                    resolve();
                } catch (e) {
                    reject(e);
                }
            }, 45000);
        });
    })();
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
