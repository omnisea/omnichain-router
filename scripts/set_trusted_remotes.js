const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

const network = 'polygon_testnet';
const networkToDeployParamsMap = {
    rinkeby: {
        id: 10121,
        address: '',
    },
    bsc_testnet: {
        id: 10102,
        address: '',
    },
    avalanche_testnet: {
        id: 10106,
        address: '0xaA89c5E42706341b66f59B687AC2c8e90F8e6eaC',
    },
    polygon_testnet: {
        id: 10109,
        address: '0xAa15132a9205028f85B8bdcf52C1A3767dAA08FD',
    },
    arbitrum_testnet: {
        id: 10143,
        address: '',
    },
    optimism_testnet: {
        id: 10132,
        address: '',
    },
    ftm_testnet: {
        id: 10112,
        address: '',
    },
    moonbeam_testnet: {
        id: 10126,
        address: '0xe08F3d854fe093AC6fdd5c3869d133a38F55a7eE',
    },
    harmony_testnet: {
        id: 10133,
        address: '',
    },

    // MAINNET NETWORKS !!! //
    // mainnet: {
    //     id: 101,
    //     address: '0x1dE48d49aeBa6Ffa4740e78c84a13de8a9c12911',
    // },
    // bsc_MAINNET: {
    //     id: 102,
    //     address: '0x7E3A34C040956C6fC8B1231ab53B355998367563',
    // },
    // avalanche_MAINNET: {
    //     id: 106,
    //     address: '0xEeb51a31685bf7385B0825139320C13Dce16f5Fc',
    // },
    // polygon_MAINNET: {
    //     id: 109,
    //     address: '0x077668085D7ba60832B7227E935C90C756501A0E',
    // },
    // arbitrum_MAINNET: {
    //     id: 110,
    //     address: '0xb9fd670E32B51188B9C44C2E2226Ec345F4debDc',
    // },
    // optimism_MAINNET: {
    //     id: 111,
    //     address: '0x5ee0753e3aB9a991849200Ff70B342053b61ef6D',
    // },
    // ftm_MAINNET: {
    //     id: 112,
    //     address: '0xF590d2958D557b98EF2c799813A2B35Bc1cEF4e4',
    // },
    // swimmer_MAINNET: {
    //     id: 114,
    //     address: '',
    // },
    // harmony_MAINNET: {
    //     id: 116,
    //     address: '0xde0F6167cec79A127e6973dAa0923aD1E87b8BCC',
    // },
    // moonbeam_MAINNET: {
    //     id: 126,
    //     address: '0x89557E29812f1967dd40E087A9f8BA0073B5DD8A',
    // },
};

async function main() {
    const localNetworkParams = networkToDeployParamsMap[network];

    if (!localNetworkParams) {
        throw new Error('Unknown network');
    }
    const contract = await hre.ethers.getContractAt('OmnichainRouter', localNetworkParams.address);

    if (!contract.address) {
        throw new Error('not deployed');
    }
    console.log("OmnichainRouter: ", contract.address, ' - adding trusted remotes');

    for (let networkName in networkToDeployParamsMap) {
        if (!networkToDeployParamsMap[networkName]) {
            throw new Error(`Unknown network: ${networkName}`);
        }
        const remoteNetworkParams = networkToDeployParamsMap[networkName];

        if (!remoteNetworkParams.address || remoteNetworkParams.id === localNetworkParams.id) {
            console.log(`Ignoring OmnichainRouter on: ${networkName}`);
            continue;
        }
        const trustedRemote = hre.ethers.utils.solidityPack(
            ['address','address'],
            [remoteNetworkParams.address, localNetworkParams.address]
        )
        const tx = await contract.setTrustedRemote(remoteNetworkParams.id, trustedRemote);

        await tx.wait();
        console.log(`Set OmnichainRouter trusted remote for ${remoteNetworkParams.id}: ${remoteNetworkParams.address}`);
    }
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
