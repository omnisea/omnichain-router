const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

const network = 'moonbeam_testnet';
const networkToDeployParamsMap = {
    rinkeby: {
        id: 10121,
        address: '',
        provider: 'lz',
        chainName: 'Ethereum',
    },
    bsc_testnet: {
        id: 10102,
        address: '',
        provider: 'lz',
        chainName: 'BSC',
    },
    avalanche_testnet: {
        id: 10106,
        address: '0xaA89c5E42706341b66f59B687AC2c8e90F8e6eaC',
        provider: 'lz',
        chainName: 'Avalanche',
    },
    polygon_testnet: {
        id: 10109,
        address: '0xAa15132a9205028f85B8bdcf52C1A3767dAA08FD',
        provider: 'lz',
        chainName: 'Polygon',
    },
    arbitrum_testnet: {
        id: 10143,
        address: '',
        provider: 'lz',
        chainName: 'Arbitrum',
    },
    optimism_testnet: {
        id: 10132,
        address: '',
        provider: 'lz',
        chainName: 'Optimism',
    },
    ftm_testnet: {
        id: 10112,
        address: '',
        provider: 'lz',
        chainName: 'Fantom',
    },
    moonbeam_testnet: {
        id: 10126,
        address: '0xe08F3d854fe093AC6fdd5c3869d133a38F55a7eE',
        provider: 'lz',
        chainName: 'Moonbeam',
    },
    harmony_testnet: {
        id: 10133,
        address: '',
        provider: 'lz',
        chainName: 'Harmony',
    },

    // MAINNET NETWORKS !!! //
    // mainnet: {
    //     address: '0x1dE48d49aeBa6Ffa4740e78c84a13de8a9c12911',
    //     provider: 'lz',
    //     id: 101,
    //     chainName: 'Ethereum',
    // },
    // bsc_MAINNET: {
    //     address: '0x7E3A34C040956C6fC8B1231ab53B355998367563',
    //     provider: 'lz',
    //     id: 102,
    //     chainName: 'BSC',
    // },
    // avalanche_MAINNET: {
    //     address: '0xEeb51a31685bf7385B0825139320C13Dce16f5Fc',
    //     provider: 'lz',
    //     id: 106,
    //     chainName: 'Avalanche',
    // },
    // polygon_MAINNET: {
    //     address: '0x077668085D7ba60832B7227E935C90C756501A0E',
    //     provider: 'lz',
    //     id: 109,
    //     chainName: 'Polygon',
    // },
    // arbitrum_MAINNET: {
    //     address: '0xb9fd670E32B51188B9C44C2E2226Ec345F4debDc',
    //     provider: 'lz',
    //     id: 110,
    //     chainName: 'Arbitrum',
    // },
    // optimism_MAINNET: {
    //     address: '0x5ee0753e3aB9a991849200Ff70B342053b61ef6D',
    //     provider: 'lz',
    //     id: 111,
    //     chainName: 'Optimism',
    // },
    // ftm_MAINNET: {
    //     address: '0xF590d2958D557b98EF2c799813A2B35Bc1cEF4e4',
    //     provider: 'lz',
    //     id: 112,
    //     chainName: 'Fantom',
    // },
    // swimmer_MAINNET: {
    //     address: '',
    //     provider: 'lz',
    //     id: 114,
    //     chainName: 'Swimmer',
    // },
    // harmony_MAINNET: {
    //     address: '0xde0F6167cec79A127e6973dAa0923aD1E87b8BCC',
    //     provider: 'lz',
    //     id: 116,
    //     chainName: 'Harmony',
    // },
    // moonbeam_MAINNET: {
    //     address: '0x89557E29812f1967dd40E087A9f8BA0073B5DD8A',
    //     provider: 'lz',
    //     id: 126,
    //     chainName: 'Moonbeam',
    // },
};

async function main() {
    const localNetworkParams = networkToDeployParamsMap[network];

    if (!localNetworkParams) {
        throw new Error('Unknown network');
    }
    const contract = await hre.ethers.getContractAt('OmnichainRouter', localNetworkParams.address);

    if (!contract.address) {
        throw new Error('not deployed');
    }
    console.log("OmnichainRouter: ", contract.address, ' - setting routes');

    for (let networkName in networkToDeployParamsMap) {
        if (!networkToDeployParamsMap[networkName]) {
            throw new Error(`Unknown network: ${networkName}`);
        }
        const remoteNetworkParams = networkToDeployParamsMap[networkName];

        if (!remoteNetworkParams.id || !remoteNetworkParams.address || !remoteNetworkParams.chainName || !remoteNetworkParams.provider || remoteNetworkParams.chainName === localNetworkParams.chainName) {
            console.log(`Ignoring OmnichainRouter on: ${networkName}`);
            continue;
        }
        const tx = await contract.setRoute(remoteNetworkParams.provider, remoteNetworkParams.id, remoteNetworkParams.chainName);

        await tx.wait();
        console.log(`Set route: ${remoteNetworkParams.chainName} with ID: ${remoteNetworkParams.id} and provider: ${remoteNetworkParams.provider}`);
    }
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
